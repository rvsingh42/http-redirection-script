#region -Config Variables-

$Rootpath = "root IIS Path"
$Site = "site name"
$offlineFileName = "AppOfflineiMagic.html"
$siteRoot = "virtual directory which is to be redirect to above file"

#endregion

#region -System Variables-

$LogPath = ".\Log"
#$DBG_LOG_FILE = "DBG_Log_"+$(GetTimestamp);
#$ERR_LOG_FILE = "ERR_Log_"+$(GetTimestamp);
$fileExistsMsg = "Same HTML file Already Exists, Press Y to overwrite and continue..."
 
$appCmdError 

#endregion

#region -Functions-
function CreateAPath {
    param (
        $path
    )
    if ($path.length -lt 1) {
        return $false
    }
    if (Test-Path -Path $path -PathType Container) {
        mkdir -Path $path -Force   
        return $true; 
    }
    return $true;
}
function PathExists {
    #-1 does not exists
    # 0 - File
    # 1 - Folder

    param($path)    
    if (Test-Path -Path $path -PathType Leaf) {    
        return 0; 
    }
    elseif (Test-Path -Path $path -PathType Container) {    
        return 0; 
    }
    else {
        return -1;
    }

}
function GetTimestamp {    
    return Get-Date -UFormat "%d%m%y%H%M";
}
function GetDownTimeInfo {    
    $downtime = Read-Host "Please Input the downtime string"
    Write-Host "Following downtime details will be udpate in the HTML File :" $downtime
    return $downtime;
}
function GetHTMLContent {    
    param($duration)
    
    if ($duration.length -lt 1) {
        throw "Downtime Details Not Provided"
        exit 0
    }

    $html = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' >
    <head>
        <title>Maintenance Mode - Outage Message</title>
    </head>
    <body>
        <h1>Maintenance Mode</h1>    
        <p>We`'re currently undergoing scheduled maintenance $duration. We will come back very shortly.</p>    
        <p>Sorry for the inconvenience!</p>        
    <!--
        Adding additional hidden content so that IE Friendly Errors don't prevent
        this message from displaying (note: it will show a 'friendly' 404
        error if the content isn't of a certain size).    
        <h2>Site under maintenance...</h2>
        <h2>Site under maintenance...</h2>
        <h2>Site under maintenance...</h2>
        <h2>Site under maintenance...</h2>
        <h2>Site under maintenance...</h2>
        <h2>Site under maintenance...</h2>
        <h2>Site under maintenance...</h2>
        <h2>Site under maintenance...</h2    
    -->
    </body>
    </html>"
    return $html
}
function EnableHTTPRedirect {
    param($destFile)
    & "$env:SystemRoot\System32\inetsrv\appcmd.exe" set config $Site -section:system.webServer/httpRedirect /enabled:'True'  /exactDestination:'True' /httpResponseStatus:'Found'    
    $arg1 = "[wildcard='/$siteRoot/*',destination='/$destFile']"
    $arg1
    & "$env:SystemRoot\System32\inetsrv\appcmd.exe" set config $Site -section:system.webServer/httpRedirect /-$arg1
    & "$env:SystemRoot\System32\inetsrv\appcmd.exe" set config $Site -section:system.webServer/httpRedirect /+$arg1
}
function DisableHTTPRedirect { 
    param ($fullPath)   
    & "$env:SystemRoot\System32\inetsrv\appcmd.exe" set config $Site -section:system.webServer/httpRedirect /enabled:'False'
    if (Test-Path -Path $fullPath -PathType Leaf) {
        try{
            Remove-Item -Path $fullPath -ErrorAction Stop
        }catch{
            Write-Host "Unable to Delete the Html file. Due to Error : $_.Exception.Message.`nKindly delete the file Manually"
            Write-Host "File Name : $_.Exception.ItemName"
            #can write to error log
        }
    }


}
function GetSystemRootPath {
    return  "$env:SystemRoot\System32"
}
function GetConfirmation {
    #  $yes = ("y", "yes")
    #  $no = ('n', "no")
    param (
        $msgTODisplay
    )    
    $retVal = $false
    $response = Read-Host $msgTODisplay
    if ($response.Length -gt 0) {  
        $response = $response.ToLower()              
        $retVal = $response -eq "y" -or $response -eq "Y" -or $response -eq "Yes"
    } 
    return $retVal
}
function CreateHTMLFile {
    param($fileContent, $rootCreatePath, $fileName)
    #check if folder exists        
    if (-Not(Test-Path -Path $rootCreatePath -PathType Container)) {
        throw "Error : Root folder Not found"
    }
    #append \ of mpt present at end
    $slashType = If ($rootCreatePath.indexOf("\") -gt -1) {"\"} Else {"/"};
    if ($rootCreatePath.EndsWith("\") -or $rootCreatePath.EndsWith("/")) {        
    }         
    else {
        $rootCreatePath += $slashType
    }
    $fullPath = $rootCreatePath + $fileName;
    if (Test-Path -Path $fullPath -PathType Leaf) {
        if (-Not(GetConfirmation $fileExistsMsg)) {
            Write-Host "Can not Continue..`nError : File Already Exists...`nPath:$fullPath"
            return $false;
        }
    } 
    # Creatr the HTML File
    $fileContent | Out-File -FilePath $fullPath -Encoding utf8 -Force

    if (Test-Path -Path $fullPath -PathType Leaf) {
        Write-Host "HTML File Created Successfully";
        return $true;
    }
    else {
        Write-Host "Error Creating HTML file...";
        return $false;
    }
}

#endregion

#region -Main Execution of the Script-
$enableMode = $true; #dont change this value manually

if ($enableMode) {
    Write-Host "Script : Enable HTTP Redirect"    
} else{
    #disable mode
    Write-Host "Script : Disable HTTP Redirect"    
}

Write-Host "Site Name : " $Site
Write-Host "Redirection File : "$offlineFileName
Write-Host "Site Root Path : "$Rootpath
if ($enableMode) {
    $dtInfo = GetDownTimeInfo
    $htmlFileContent = GetHTMLContent $dtInfo
    if (-Not(CreateHTMLFile $htmlFileContent $Rootpath $offlineFileName)) {
        Write-Host "Error in CreateHTMLFile!"
        return
    }
    EnableHTTPRedirect $offlineFileName
    Write-Host "Enabled Successfully"
} else{
    #disable mode    
    $fPath = $Rootpath+"\"+$offlineFileName
    DisableHTTPRedirect  $fPath
    Write-Host "Disabled Successfully"
}

#Endregion