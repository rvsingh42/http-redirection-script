Usage
---
```Powershell
$Rootpath = "Path to root folder of the Configured Site"
$Site = "Name of the Site"
$offlineFileName = "name of the html file which will contain the message for users"
$siteRoot = "Name of virtual directory which is to be taken offline / shown downtime message for"
```
1. Run this script with **Powershell** ( must '*run as admin*' )
2. When Run,Will ask for the Message that is to be displayed to user ( duration of the downtime can/must be mentioned here ).


Example
---
```Powershell
$Rootpath = "C:\Users\Ravi G Singh\Documents\IDEA_RAS_WEB\IIS"
$Site = "IDEA_RAS"
$offlineFileName = "AppOffline.html"
$siteRoot = "ras"
```

Dependencies
---

It has only one Dependency i.e. ***HTTP Redirection*** Module must be enabled.
Please follow below link to for help on how to verify/enable HTTP Redirection. Module.

[HTTP Redirection Setup](https://docs.microsoft.com/en-us/iis/configuration/system.webserver/httpredirect/#setup)